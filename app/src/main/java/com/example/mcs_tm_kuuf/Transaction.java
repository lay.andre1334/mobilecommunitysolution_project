package com.example.mcs_tm_kuuf;

public class Transaction {


    private int userId;
    private String productName;
    private String transactionDate;
    private int productPrice;

    public Transaction(int userId, String productName, String transactionDate, int productPrice) {
        this.userId = userId;
        this.productName = productName;
        this.transactionDate = transactionDate;
        this.productPrice = productPrice;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }


    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public int getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(int productPrice) {
        this.productPrice = productPrice;
    }


}
