package com.example.mcs_tm_kuuf;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import java.net.Inet4Address;
import java.util.Vector;


public class HomeActivity extends AppCompatActivity {

    public static final String SEND_USERID = "com.example.application.MCS_TM_KUUF.SEND_USERID";
    int userId;
    TextView tvGreet, tvBalance, tvEmpty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //GETTING LOGGED USER'S ID
        Intent intent = getIntent();
        userId = intent.getIntExtra(LoginActivity.SEND_USERID,0);

        //GREET
        tvGreet = findViewById(R.id.tvGreet);
        String greet = "Welcome " + (String) LoginActivity.vecUser.get(userId).getUsername();
        tvGreet.setText(greet);

    }

    //MENU ITEM SECTION
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menuoption, menu);
        return true;

    }
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId()==R.id.storeMenu){
            Intent intent = new Intent(this, StoreActivity.class);
            intent.putExtra(SEND_USERID,userId);
            startActivity(intent);
        } else if (item.getItemId() == R.id.profileMenu) {
            Intent intent = new Intent(this, ProfileActivity.class);
            intent.putExtra(SEND_USERID,userId);
            startActivity(intent);
        } else if (item.getItemId() == R.id.logoutMenu) {
            Intent intent = new Intent(this,LoginActivity.class);
            startActivity(intent);
            finish();
        } else if (item.getItemId() == R.id.homeMenu){
            startActivity(getIntent());
            overridePendingTransition(0,0);
            finish();
            overridePendingTransition(0,0);
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        //FOR UPDATE WALLET
        int wallet = LoginActivity.vecUser.get(userId).getWallet();
        tvBalance = findViewById(R.id.tvBalance);
        String balance = "Your balance: Rp. " + String.valueOf(wallet);
        tvBalance.setText(balance);

        //VALIDATE EMPTY TRANSACTION
        tvEmpty = findViewById(R.id.tvEmpty);
        User user = LoginActivity.vecUser.get(userId);
        Vector<Transaction> transVec = user.getVecTrans();
        if(transVec.isEmpty()){
            tvEmpty.setText("Currently, you have no transaction");
        }else{
            tvEmpty.setText(null);
        }

        //RECYCLER VIEW FOR TRANSACTION HISTORY
        RecyclerView rvTransaction = findViewById(R.id.rvTransaction);
        TransactionAdapter transactionAdapter = new TransactionAdapter(this);
        transactionAdapter.setUserId(userId);
        rvTransaction.setAdapter(transactionAdapter);
        rvTransaction.setLayoutManager(new GridLayoutManager(this,1));

        //REFRESH PAGE IF TRANSACTION DELETED
        if(TransactionAdapter.removed == 1){
            TransactionAdapter.removed = 0;
            finish();
            overridePendingTransition(0,0);
        }
    }

}