package com.example.mcs_tm_kuuf;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

public class ProductDetailActivity extends AppCompatActivity {


    Button btnBuy;
    TextView tvDetailName, tvDetailMin, tvDetailMax, tvDetailPrice;
    static Vector<Transaction> vecTrans = new Vector<>();
    static int finish = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        // GET PRODUCT ID
        Intent intent = getIntent();
        final int productId = intent.getIntExtra(ProductAdapter.SEND_PRODUCTID,0);
        final int userId = intent.getIntExtra(ProductAdapter.SEND_USERID,0);

        // SHOW PRODUCT DETAIL
        tvDetailName = findViewById(R.id.tvDetailName);
        tvDetailName.setText(StoreActivity.vecProduct.get(productId).getProductName());

        tvDetailMin = findViewById(R.id.tvDetailMin);
        tvDetailMin.setText("Minimal Player: " + StoreActivity.vecProduct.get(productId).getMinPlayer());

        tvDetailMax = findViewById(R.id.tvDetailMax);
        tvDetailMax.setText("Maximal Player: " + StoreActivity.vecProduct.get(productId).getMaxPlayer());

        tvDetailPrice = findViewById(R.id.tvDetailPrice);
        tvDetailPrice.setText("Price: " + StoreActivity.vecProduct.get(productId).getPrice());

        //BUY PRODUCT
        btnBuy = findViewById(R.id.btnBuy);
        btnBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int wallet = LoginActivity.vecUser.get(userId).getWallet();
                int price = StoreActivity.vecProduct.get(productId).getPrice();
                if(wallet >= price){

                    Toast.makeText(getApplicationContext(),"Purchased",Toast.LENGTH_LONG).show();

                    //SET NEW BALANCE
                    int newBalance = wallet - price;
                    LoginActivity.vecUser.get(userId).setWallet(newBalance);

                    //ADD TRANSACTION TO VECTOR TRANSACTION
                    String productName = StoreActivity.vecProduct.get(productId).getProductName();
                    String date = new SimpleDateFormat("dd MMMM yyyy").format(Calendar.getInstance().getTime());
                    Transaction transaction = new Transaction(userId,productName,date,price);
                    Vector<Transaction> vecUser = new Vector<>();
                    User userVec = LoginActivity.vecUser.get(userId);
                    userVec.getVecTrans().add(transaction);

                    //VALIDATE FINISH FOR StoreActivity
                    finish = 1;
                    finish();

                }else{
                    Toast.makeText(getApplicationContext(),"Not enough balance",Toast.LENGTH_LONG).show();
                    finish = 1;
                    finish();
                }
            }
        });
    }
}