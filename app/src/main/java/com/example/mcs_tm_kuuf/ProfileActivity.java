package com.example.mcs_tm_kuuf;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class ProfileActivity extends AppCompatActivity {

    TextView tvProfileName, tvProfileGender, tvProfilePhone, tvProfileDOB, tvProfileWallet, tvProfileTopUp;
    RadioGroup rgTopUp;
    RadioButton radioButton, rb250, rb500, rb1000;
    Button btnConfirm;
    EditText txtConfirmTopUp;
    private int topUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Intent intent = getIntent();
        final int userId = intent.getIntExtra(HomeActivity.SEND_USERID,0);
        Log.i("userData", "ReceivedFromHome: " + userId);

        //SET USER DATA PROFILE
        tvProfileName = findViewById(R.id.tvProfileName);
        tvProfileName.setText("Username: " + LoginActivity.vecUser.get(userId).getUsername());

        tvProfileGender = findViewById(R.id.tvProfileGender);
        tvProfileGender.setText("Gender: " + LoginActivity.vecUser.get(userId).getGender());

        tvProfilePhone = findViewById(R.id.tvProfilePhone);
        tvProfilePhone.setText("Phone Number: " + LoginActivity.vecUser.get(userId).getPhone());

        tvProfileDOB = findViewById(R.id.tvProfileDOB);
        tvProfileDOB.setText("Date of birth: " + LoginActivity.vecUser.get(userId).getDOB());

        tvProfileWallet = findViewById(R.id.tvProfileWallet);
        tvProfileWallet.setText("Your wallet: Rp. " + LoginActivity.vecUser.get(userId).getWallet());

        //TOP UP SECTION
        rgTopUp = findViewById(R.id.rgTopUp);
        tvProfileTopUp = findViewById(R.id.tvProfileTopUp);
        btnConfirm = findViewById(R.id.btnConfirm);
        txtConfirmTopUp = findViewById(R.id.txtConfirmTopUp);
        tvProfileTopUp = findViewById(R.id.tvProfileTopUp);

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean flag = false;

                //VALIDATE RADIOBUTTON TOPUP
                rb250 = findViewById(R.id.rb250);
                rb500 = findViewById(R.id.rb500);
                rb1000 = findViewById(R.id.rb1000);
                if(!rb250.isChecked() && !rb500.isChecked() && !rb1000.isChecked()){
                    tvProfileTopUp.setError("Choose nominal");
                    Toast.makeText(getApplicationContext(),"Choose nominal",Toast.LENGTH_LONG).show();
                    flag = true;
                }else{
                    int radioId;
                    radioId = rgTopUp.getCheckedRadioButtonId();
                    radioButton = findViewById(radioId);
                    topUp = Integer.valueOf(radioButton.getText().toString());
                    tvProfileTopUp.setError(null);
                }

                //VALIDATE PASSWORD
                String password = txtConfirmTopUp.getText().toString();
                if(!password.equals(LoginActivity.vecUser.get(userId).getPassword())){
                    txtConfirmTopUp.setError("Wrong Password");
                    flag = true;
                }
                if(password.isEmpty()){
                    txtConfirmTopUp.setError("Password must be filled");
                    flag = true;
                }

                //ALL CONFIRMED
                if(!flag){
                    int wallet = LoginActivity.vecUser.get(userId).getWallet();
                    wallet = wallet + topUp;
                    LoginActivity.vecUser.get(userId).setWallet(wallet);
                    Toast.makeText(getApplicationContext(),"Top up success", Toast.LENGTH_LONG).show();
                    finish();
                }

            }
        });

    }
}