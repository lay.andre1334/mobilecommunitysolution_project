package com.example.mcs_tm_kuuf;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import java.util.Vector;

public class StoreActivity extends AppCompatActivity {

    static Vector<Product> vecProduct = new Vector<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);

        //ADD DATA TO PRODUCT VECTOR
        if(vecProduct.isEmpty()){
            vecProduct.add(new Product(0, "Exploding Kitten",
                    2,5,250000,"106.265139","-6.912035"));

            vecProduct.add(new Product(1, "Card Against Humanity",
                    2,4,182500,"108.126810","-7.586037"));
        }

        Intent intent = getIntent();
        int userId = intent.getIntExtra(HomeActivity.SEND_USERID,0);

        //RECYCLER VIEW
        RecyclerView rvProduct = findViewById(R.id.rvProduct);
        ProductAdapter productAdapter = new ProductAdapter(this);
        productAdapter.setVecProduct(vecProduct);
        productAdapter.setUserId(userId);

        rvProduct.setAdapter(productAdapter);
        rvProduct.setLayoutManager(new GridLayoutManager(this,1));

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(ProductDetailActivity.finish == 1){
            ProductDetailActivity.finish = 0;
            finish();
        }
    }
}