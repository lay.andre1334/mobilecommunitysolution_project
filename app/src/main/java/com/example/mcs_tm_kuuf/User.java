package com.example.mcs_tm_kuuf;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Vector;

public class User {

    private long id;
    private String username;
    private String password;
    private String phone;
    private String DOB;
    private String gender;
    private int wallet;
    private Vector<Transaction> vecTrans = new Vector<>();

    public User(long id, String username, String password, String phone, String DOB, String gender, int wallet, Vector<Transaction> vecTrans) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.phone = phone;
        this.DOB = DOB;
        this.gender = gender;
        this.wallet = wallet;
        this.vecTrans = vecTrans;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getWallet() {
        return wallet;
    }

    public void setWallet(int wallet) {
        this.wallet = wallet;
    }

    public Vector<Transaction> getVecTrans() {
        return vecTrans;
    }

    public void setVecTrans(Vector<Transaction> vecTrans) {
        this.vecTrans = vecTrans;
    }

}
