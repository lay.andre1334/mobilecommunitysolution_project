package com.example.mcs_tm_kuuf;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Vector;

public class LoginActivity extends AppCompatActivity {
    EditText txtUsername;
    EditText txtPassword;
    Button btnLogin;
    Button btnRegister;
    static Vector<User> vecUser = new Vector<>();
    public static final String SEND_USERID = "com.example.application.MCS_TM_KUUF.SEND_USERID";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtUsername = findViewById(R.id.txtUsername);
        txtPassword = findViewById(R.id.txtPassReg);
        btnLogin = findViewById(R.id.btnLogin);
        btnRegister = findViewById(R.id.btnRegister);

        //ADMIN ACCOUNT
        if(vecUser.isEmpty()){
            Vector<Transaction> vecTrans = new Vector<>();
            User user = new User(0,"andre","andre",
                    "0821","13-03-2001","Male",1000000,vecTrans);
            vecUser.add(user);
            Log.i("tes","vecSize" + vecUser.size());
        }

        //LOGIN VALIDATION
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String username = txtUsername.getText().toString();
                String password = txtPassword.getText().toString();

                if(vecUser.isEmpty()){
                    if(username.length() == 0)txtUsername.setError("Username must be filled!");
                    if(password.length() == 0)txtPassword.setError("Password must be filled!");
                    else{
                        txtUsername.setError("Wrong Username or Password");
                        txtPassword.setError("Wrong Username or Password");
                    }
                }else{
                    for(int i=0 ; i<vecUser.size() ; i++){
                        if(username.equals(vecUser.get(i).getUsername()) && password.equals(vecUser.get(i).getPassword())){
                            Toast.makeText(getBaseContext(), "Welcome", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                            intent.putExtra(SEND_USERID,i);
                            Log.i("userData","sentID: " + vecUser.get(i).getId());
                            startActivity(intent);
                            finish();
                            break;
                        }
                        if(i == vecUser.size()-1){
                            txtUsername.setError("Wrong Username or Password");
                            txtPassword.setError("Wrong Username or Password");
                        }
                    }
                }

                if(username.isEmpty()){
                    txtUsername.requestFocus();
                    txtUsername.setError("Username must be filled!");
                }
                if(password.isEmpty()){
                    txtPassword.requestFocus();
                    txtPassword.setError("Password must be filled!");
                }
            }
        });

        //TO REGISTER PAGE
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(intent);
            }
        });
    }
}
