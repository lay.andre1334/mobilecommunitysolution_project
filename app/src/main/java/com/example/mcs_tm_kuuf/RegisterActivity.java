package com.example.mcs_tm_kuuf;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Vector;

public class RegisterActivity extends AppCompatActivity {

    public static final String SEND_DATA = "com.example.application.MCS_TM_KUUF.SEND_DATA";

    //USERNAME VARIABLE
    EditText txtUserReg;

    //PASSWORD VARIABLE
    EditText txtPassReg;
    EditText txtPassConfirm;

    //Phone
    EditText txtPhone;

    //DATE PICKER VARIABLE
    TextView txtDate;
    ImageView calender;
    private int date,month,year;
    private String username, password, phoneNumber, DOB, gender;

    //GENDER VARIABLE
    RadioGroup radioGroup;
    RadioButton radioButton;
    RadioButton rbMale, rbFemale;
    TextView tvGender;

    //TERMS VARIABLE
    CheckBox checkBox;

    Button btnRegistered;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        txtUserReg = findViewById(R.id.txtUserReg);
        txtPassReg = findViewById(R.id.txtPassReg);
        txtPassConfirm = findViewById(R.id.txtPassConfirm);
        txtPhone = findViewById(R.id.txtPhone);
        txtDate = findViewById(R.id.txtDate);
        calender = findViewById(R.id.datePicker);
        tvGender = findViewById(R.id.tvGender);
        radioGroup = findViewById(R.id.radioGroup);
        checkBox = findViewById(R.id.checkBox);
        btnRegistered = findViewById(R.id.btnRegistered);

        //DATE PICKER BUTTON
        calender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cal = Calendar.getInstance() ;
                date = cal.get(Calendar.DAY_OF_MONTH);
                month = cal.get(Calendar.MONTH);
                year = cal.get(Calendar.YEAR);

                DatePickerDialog datepick = new DatePickerDialog(RegisterActivity.this, android.R.style.Theme_DeviceDefault_Light_Dialog
                        , new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int date) {
                        month+=1;
                        DOB = date + " - " + month + " - " + year;
                        txtDate.setText(DOB);
                    }
                },
                        date, month, year);
                datepick.updateDate(2020, 1, 1);
                datepick.show();
            }
        });

        //VALIDATION
        btnRegistered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean flag = false;
                //USERNAME VALIDATION
                username = txtUserReg.getText().toString();
                if(username.length() < 6 || username.length() > 12 ){
                    txtUserReg.setError("Username must be between 6 and 12 characters");
                    flag = true;
                }
                if(username.isEmpty()){
                    txtUserReg.setError("Field cannot be empty");
                    flag = true;
                }

                //PASSWORD VALIDATION
                password = txtPassReg.getText().toString();
                String passConfirm = txtPassConfirm.getText().toString();
                if(!password.matches("^(?=.*[a-zA-Z])(?=.*[0-9])[A-Za-z0-9]+$")){
                    txtPassReg.setError("Password must be alphanumeric");
                    flag = true;
                }
                if(password.length() <= 8){
                    txtPassReg.setError("Password length must be more than 8 characters");
                    flag = true;
                }
                if(password.isEmpty()){
                    txtPassReg.setError("Field cannot be empty");
                    flag = true;
                }

                if(passConfirm.isEmpty()){
                    txtPassConfirm.setError("Field cannot be empty");
                    flag = true;
                }
                if(!passConfirm.equals(password)){
                    txtPassConfirm.setError("Password didn't match");
                    flag = true;
                }

                //VALIDATE PHONE
                phoneNumber = txtPhone.getText().toString();
                if(phoneNumber.length()<10 || phoneNumber.length()>12){
                    txtPhone.setError("Phone number must be between 10 and 12 characters");
                    flag = true;
                }
                if(phoneNumber.isEmpty()){
                    txtPhone.setError("Field cannot be empty");
                    flag = true;
                }

                //VALIDATE DATE
                if(txtDate.getText().length() == 0) {
                    txtDate.setError("DOB cannot be empty");
                    flag = true;
                }else {
                    txtDate.setError(null);
                }

                //VALIDATE RADIO BUTTON GENDER
                rbMale = findViewById(R.id.rbMale);
                rbFemale = findViewById(R.id.rbFemale);
                Log.i("test","test");
                if (!rbMale.isChecked() && !rbFemale.isChecked()) {
                    tvGender.setError("Choose gender");
                    flag = true;
                }else{
                    int radioId;
                    radioId = radioGroup.getCheckedRadioButtonId();
                    radioButton = findViewById(radioId);
                    gender = radioButton.getText().toString();
                    tvGender.setError(null);
                }

                //VALIDATE TERMS AND CONDITION
                if(!checkBox.isChecked()){
                    checkBox.setError("You must agree to the terms and condition");
                    flag = true;
                }else{
                    checkBox.setError(null);
                }

                //VALIDATE ALL VARIABLE
                if(flag){
                    Toast.makeText(getBaseContext(), "Please recheck your data", Toast.LENGTH_LONG).show();
                }else{
                    int id = LoginActivity.vecUser.size() ;
                    Toast.makeText(getBaseContext(), "Register Success", Toast.LENGTH_LONG).show();
                    Vector<Transaction> vecTrans = new Vector<>();
                    User user = new User(id, username, password, phoneNumber, DOB, gender,0,vecTrans);
                    LoginActivity.vecUser.add(user);
//                    Log.i("userData","UserID: " + id);
//                    Log.i("userData","Username: " + username);
//                    Log.i("userData","Password: " + password);
//                    Log.i("userData","Phone: " + phoneNumber);
//                    Log.i("userData","DOB: " + DOB);
//                    Log.i("userData","Gender: " + gender);
                    finish();
                }
            }
        });

    }
}
