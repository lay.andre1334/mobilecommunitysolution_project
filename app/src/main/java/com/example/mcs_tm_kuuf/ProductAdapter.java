package com.example.mcs_tm_kuuf;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Vector;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    int userId;
    Context ctx;
    Vector<Product> vecProduct = new Vector<>();
    public static final String SEND_USERID = "com.example.application.MCS_TM_KUUF.SEND_USERID";
    public static final String SEND_PRODUCTID = "com.example.application.MCS_TM_KUUF.SEND_PRODUCTID";

    private OnItemClickListener productListener;

    public  interface  OnItemClickListener{
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        productListener = listener;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setVecProduct(Vector<Product> vecProduct) {
        this.vecProduct = vecProduct;
    }
    public ProductAdapter(Context ctx) {
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public ProductAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.product_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ProductAdapter.ViewHolder holder, final int position) {
        //SET DATA
        holder.tvProductName.setText("Product Name: " + vecProduct.get(position).getProductName());
        holder.tvMinPlayer.setText(String.valueOf("Min Player: " + vecProduct.get(position).getMinPlayer()));
        holder.tvMaxPlayer.setText(String.valueOf("Max Player: " + vecProduct.get(position).getMaxPlayer()));
        holder.tvProductPrice.setText(String.valueOf("Price: Rp. " + vecProduct.get(position).getPrice()));

        holder.cvProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, ProductDetailActivity.class);
                intent.putExtra(SEND_PRODUCTID,vecProduct.get(position).getProductId());
                intent.putExtra(SEND_USERID,userId);
                ctx.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return StoreActivity.vecProduct.size();
    }

    class ViewHolder extends  RecyclerView.ViewHolder{

        TextView tvProductName, tvMinPlayer, tvMaxPlayer, tvProductPrice;
        CardView cvProduct;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvProductName = itemView.findViewById(R.id.tvProductName);
            tvMinPlayer = itemView.findViewById(R.id.tvMinPlayer);
            tvMaxPlayer = itemView.findViewById(R.id.tvMaxPlayer);
            tvProductPrice = itemView.findViewById(R.id.tvProductPrice);
            cvProduct = itemView.findViewById(R.id.productCardView);
        }
    }
}
