package com.example.mcs_tm_kuuf;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Vector;

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.ViewHolder> {

    Context ctx;
    Vector<Transaction> transVec = new Vector<>();
    User userVec;
    int userId;
    static int removed = 0;


//    public int getRemoved() {
//        return removed;
//    }
//
//    public void setRemoved(int removed) {
//        this.removed = removed;
//    }


    public void setUserId(int userId) {
        this.userId = userId;
    }

    public TransactionAdapter(Context ctx) {
        this.ctx = ctx;
    }

//    public void setVecTrans(Vector<Transaction> vecTrans) {
//        this.vecTrans = vecTrans;
//    }

    @NonNull
    @Override
    public TransactionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.transaction_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TransactionAdapter.ViewHolder holder, final int position) {
        //set data here
        userVec = LoginActivity.vecUser.get(userId);
        transVec = userVec.getVecTrans();
        holder.tvTransName.setText("Product Name: " + transVec.get(position).getProductName());
        holder.tvTransactionDate.setText("Transaction Date: " + transVec.get(position).getTransactionDate());
        holder.tvTransPrice.setText("Price: Rp. " + transVec.get(position).getProductPrice());

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transVec.remove(position);
                removed = 1;
                Toast.makeText(ctx,"Transaction Removed", Toast.LENGTH_LONG).show();
                Intent refresh = new Intent(ctx,HomeActivity.class);
                ctx.startActivity(refresh);
            }
        });
    }

    @Override
    public int getItemCount() {
        userVec = LoginActivity.vecUser.get(userId);
        transVec = userVec.getVecTrans();
        return transVec.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvTransName, tvTransactionDate, tvTransPrice;
        Button btnDelete;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            //get data here
            tvTransName = itemView.findViewById(R.id.tvTransName);
            tvTransactionDate = itemView.findViewById(R.id.tvTransactionDate);
            tvTransPrice = itemView.findViewById(R.id.tvTransPrice);
            btnDelete = itemView.findViewById(R.id.btnDelete);
        }
    }

}
